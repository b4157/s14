/*Demo*/
document.getElementById("btn-1").addEventListener('click', () => {
		alert("Add more!");
});

let paragraph = document.getElementById("paragraph-1");
let paragraph2 = document.getElementById("paragraph-2");

document.getElementById("btn-2").addEventListener('click', () => {
		paragraph.innerHTML = "I can even do this!";
});

document.getElementById("btn-3").addEventListener('click', () => {
		paragraph2.innerHTML = "Or this!";
		paragraph2.style.color = "red";
		paragraph2.style.fontSize = "50px";
});


console.log("Hello World!");

/*
	JavaScript
		-	we see of log message in our console

			Consoles are part of our browser which will allow us to see/log
			messages, data or information from our programming langauage.

			In fact,  console can be accessed through its developer tools
			in console tab. For most browser allow us to add some javascript
			expession.

			Statments
				- Statements are instructions, expression we add to our
				 programming language  which will then be communicated to our 
				 computers.
				- Statements in JavaScript commonly ends with semicolon(;)
				- semicolons marks the end of statements.

			Syntax
				- Syntax in programming is a set of rules 

*/

console.log("Ailen Laguda");

// Varialbles
let num =10;
// let keyword is for create variable. num is our variable. 
console.log(num);

let myVariable;
// You can always initialize after variable declaration 
myVariable="New initialized value";
console.log(myVariable);

myVariable=7;
console.log(myVariable);

/*You cannot and should not access a variable before it's been created/
declared*/

myVariable2 = "Initial 2";
console.log(myVariable2);


check = myVariable2 + myVariable;
console.log(check);

let besfinalfinatasy;
besfinalfinatasy = "Final Finanty 6";
console.log(besfinalfinatasy)


const pi=3.1416;
console.log(pi);

const mvp= "Michale Jordan";
console.log(typeof(mvp));



/*	Mini Activity
	- Create a group of data. Should contain names from favorite band 
	- Create a variable which contain multiple values of differing 
*/
let favband = ["kutless", "planetshakers", "imagine dragons"];
console.log(favband);

let student1 = {
	firstName: "Ailen",
	lastName: "Laguda",
	isDeveloper: true,
	hasPortfolio: true,
	age: 32

};

console.log(student1);


function display(data){
	console.log(data + " is Fun!")
}

display("Data");
display("JavaScript");
display("Reading");


function displayFullName(firstName,lastName,age) {
	console.log("Hi" + firstName + " "+ lastName);
	console.log	("You are " + age + " years old");
	// body...
}

displayFullName("Ailen","Laguda",25);


let sample1 = "This is a sample";
const sampleconst = "Sample Constant";

function sampleFunc(){
	console.log(sample1);
	console.log(sampleconst);

}

sampleFunc();

function addition(x,y){
	return x+y;
}

console.log(addition(1,2));
