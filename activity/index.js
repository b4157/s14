/*
	Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
*/

function Add2Num(a,b) {
	return a + b;
	// body...
}

console.log(Add2Num(1,2));


/*
	Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
*/

function Sub2Num(c,d) {
	return c - d;
	// body...
}

console.log(Sub2Num(5,3));

/*
	Create function which will be able to multiply two numbers.
		-Numbers must be provided as arguments.
		-Return the result of the multiplication.
*/

function Prod2Num(e,f) {
	return e * f;
	// body...
}

/*
	-Create a new variable called product.
		-This product should be able to receive the result of multiplication function.

	Log the value of product variable in the console.

*/

let product = Prod2Num(10,5);
console.log(product)